# coding: utf-8
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from apostas_boloes.apostas.forms import JogoBolaoForm
from apostas_boloes.apostas.models import ApostaLoteria, JogoLoteria, JogoBolao, ApostaBolao
from apostas_boloes.adesao.models import Participante
from datetime import datetime
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required




def apostas_loteria(request):
    # Lista as apostas válidas e coloca a opção para ousuário escolher e apostar
    
    apostas = ApostaLoteria.objects.filter(ativo=True, 
        data_final__gte=datetime.now())
    return render(request, 'apostas/apostaLoteria.html', {
        'apostas': apostas,
    })

def apostar_loteria(request, aposta_id):
    aposta = ApostaLoteria.objects.get(pk=aposta_id)
    participante = Participante.objects.get(user_id=request.user.pk)
    jogo = JogoLoteria(participante=participante, aposta=aposta)
    jogo.save()

    return HttpResponse()

def apostas_bolao(request):
    apostas = ApostaBolao.objects.filter(ativo=True, 
        data_final__gte=datetime.now())
    return render(request, 'apostas/apostaBolao.html', {
        'apostas': apostas,
    })

def apostar_bolao(request, bolao_id):
    bolao = ApostaBolao.objects.get(pk=bolao_id)
    if request.method == 'POST':
        form = JogoBolaoForm(request.POST, timeA=bolao.timeA, timeB=bolao.timeB)
        if form.is_valid():
            print form.cleaned_data
            jogo = JogoBolao()
            jogo.gols_timeA = form.cleaned_data['gols_timeA']
            jogo.gols_timeB = form.cleaned_data['gols_timeB']
            jogo.aposta = bolao
            jogo.participante = Participante.objects.get(user_id=request.user.pk)
            jogo.save()
            msg = 'Aposta no jogo "%s" Efetuada!' % bolao
            request.session['msg'] = msg
            return HttpResponseRedirect(reverse('apostas:apostas_bolao'))
    else:
        form = JogoBolaoForm(timeA=bolao.timeA, timeB=bolao.timeB) # An unbound form

    return render(request, 'apostas/apostaJogoBolao.html', {
        'form': form, 'bolao': bolao
    })

@login_required
def minhas_apostas(request):
    return HttpResponse()
    