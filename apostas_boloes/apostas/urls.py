from django.conf.urls import patterns, include, url
from apostas_boloes import apostas


urlpatterns = patterns('apostas_boloes.apostas.views',
    url(r'^apostasLoteria$', 'apostas_loteria', name='apostas_loteria'),
    url(r'^apostar_loteria/(\d+)/$', 'apostar_loteria', name='apostar_loteria'),
    url(r'^apostasBolao$', 'apostas_bolao', name='apostas_bolao'),
    url(r'^apostar_bolao/(\d+)/$', 'apostar_bolao', name='apostar_bolao'),
    url(r'^minhas_apostas', 'minhas_apostas', name='minhas_apostas'),
)