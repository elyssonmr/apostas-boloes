from django import forms
from apostas_boloes.apostas.models import JogoBolao


class JogoBolaoForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		timeA = kwargs.pop('timeA')
		timeB = kwargs.pop('timeB')
		super(JogoBolaoForm, self).__init__(*args, **kwargs)
		self.fields['gols_timeA'].label = timeA
		self.fields['gols_timeB'].label = timeB

	class Meta:
		model = JogoBolao
		exclude = ('pago', 'aposta', 'participante')