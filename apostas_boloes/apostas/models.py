# coding: utf-8
from django.db import models
from apostas_boloes.adesao.models import Participante

class Jogo(models.Model):
	pago = models.BooleanField('Já Pagou?', default=False)
	participante = models.ForeignKey(Participante)	
	#data_pagamento = models.DateField('Data Pagamento')
	
	class Meta:
		abstract = True			

class JogoLoteria(Jogo):
	aposta = models.ForeignKey('ApostaLoteria')

	def __unicode__(self):
		return '%s apostou %.2f em %s' % \
		(self.participante.nome, self.aposta.valor, self.aposta.nome)

	class Meta:
		verbose_name = 'Jogo Loteria'
		verbose_name_plural = 'Jogos Loteria'
			

class JogoBolao(Jogo):
	aposta = models.ForeignKey('ApostaBolao')
	gols_timeA = models.PositiveSmallIntegerField('TimeA', default=0)
	gols_timeB = models.PositiveSmallIntegerField('TimeB', default=0)

	def __unicode__(self):
		return '%s aposto %d gols no %s e %d gols no %s' % \
		(self.participante.nome, self.gols_timeA, self.aposta.timeA, \
			self.gols_timeB, self.aposta.timeB)

	class Meta:
		verbose_name = 'Jogo Bolão'
		verbose_name_plural = 'Jogos Bolão'
			

class Aposta(models.Model):
	nome = models.CharField('Nome', max_length=50)
	descricao = models.TextField('Descrição', max_length=100)
	valor = models.DecimalField('Valor da Aposta', max_digits=10, decimal_places=2)
	ativo = models.BooleanField('Ativado', default=True)
	data_final = models.DateField('Data Final para Apostas', blank = True)

	def __unicode__(self):
		return self.nome
	
	class Meta:
		abstract = True


class ApostaLoteria(Aposta):
	data_sorteio = models.DateField('Data Sorteio', auto_now = True)		
	participantes = models.ManyToManyField(Participante, through='JogoLoteria')

	class Meta:
		verbose_name = 'Aposta Loteria'
		verbose_name_plural = 'Apostas Loteria'
			

class ApostaBolao(Aposta):
	timeA = models.CharField('Time A', max_length=50)
	timeB = models.CharField('Time B', max_length=50)
	participantes = models.ManyToManyField(Participante, through='JogoBolao')

	def __unicode__(self):
		return '%s x %s' % (self.timeA, self.timeB)

	class Meta:
		verbose_name = 'Bolão'
		verbose_name_plural = 'Bolões'