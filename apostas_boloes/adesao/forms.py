from django import forms
from apostas_boloes.adesao.models import Participante


class AdesaoForm(forms.ModelForm):
	nome = forms.CharField(max_length=50)
	email = forms.EmailField(max_length=50)
	telefone = forms.CharField(max_length=15)
	login = forms.CharField(max_length=30)
	senha = forms.CharField(max_length=30, widget=forms.PasswordInput())

	class Meta:
		model = Participante
		exclude = ['user']
