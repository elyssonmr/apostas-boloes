# coding: utf-8
from django.contrib import admin
from django.utils.translation import ungettext, gettext as _
from apostas_boloes.adesao.models import Participante, Setor
from django.contrib.auth.models import User



class SetorAdmin(admin.ModelAdmin):
	search_fields = ('nome',)

class ParticipanteAdmin(admin.ModelAdmin):
	list_display = ('nome', 'setor', 'email', 'telefone',)
	search_fields = ('nome', 'telefone', 'email',)
	list_filter = ('setor', 'nome',)

admin.site.register(Setor, SetorAdmin)
admin.site.register(Participante, ParticipanteAdmin)