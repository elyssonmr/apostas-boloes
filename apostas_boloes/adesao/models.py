# coding: utf-8
from django.db import models
from django.contrib.auth.models import User


class Setor(models.Model):
	nome = models.CharField('Nome Setor', max_length=50)

	def __unicode__(self):
		return self.nome

	class Meta:
		verbose_name = 'Setor'
		verbose_name_plural = 'Setores'

class Participante(models.Model):
	nome = models.CharField('Nome', max_length=50)
	email = models.EmailField('E-mail', max_length=50)
	telefone = models.CharField('Telefone/Ramal', max_length=50)
	setor = models.ForeignKey(Setor)
	user = models.OneToOneField(User)

	def __unicode__(self):
		return '%s - %s' % (self.nome, self.setor)

	class Meta:
		verbose_name = 'Participante'
		verbose_name_plural = 'Participantes'
	