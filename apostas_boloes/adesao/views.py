from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from apostas_boloes.adesao.forms import AdesaoForm
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from apostas_boloes.adesao.models import Participante

def adesao(request):
    if request.method == 'POST':
        form = AdesaoForm(request.POST)
        if form.is_valid():
            nome = form.cleaned_data['nome']
            login = form.cleaned_data['login']
            senha = form.cleaned_data['senha']
            email = form.cleaned_data['email']            
            telefone = form.cleaned_data['telefone']
            setor = form.cleaned_data['setor']

            user = User.objects.create_user(login, email, senha)
            user.name = nome
            user.save()
            participante = Participante(telefone=telefone, user=user, setor=setor)
            participante.save()

            return HttpResponseRedirect(reverse('core:home'))
    else:
        form = AdesaoForm() # An unbound form

    return render(request, 'adesao/adesao.html', {
        'form': form,
    })