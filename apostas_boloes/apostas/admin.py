# coding: utf-8
from django.contrib import admin
from django.utils.translation import ungettext, gettext as _
from apostas_boloes.apostas.models import ApostaLoteria, JogoLoteria, JogoBolao, ApostaBolao
from django.utils.datetime_safe import datetime

class ApostaLoteriaAdmin(admin.ModelAdmin):
	actions = ['encerrar_aposta']
	list_display = ('nome', 'descricao', 'ativo', 'valor', 'data_final', 'qtd_participantes', 'total')

	def qtd_participantes(self, obj):
		return len(JogoLoteria.objects.filter(aposta__pk=obj.pk, pago=True))
	
	qtd_participantes.short_description = 'Participantes Pagantes'

	def total(self, obj):
		return '%d R$' % (obj.valor * self.qtd_participantes(obj))

	total.short_description = 'Total das Apostas'

	def encerrar_aposta(self, request, queryset):
		count = queryset.update(ativo=False, data_final=datetime.today().date())
		msg = ungettext(
			u'%d Aposta Encerrada!',
			u'%d Apostas Encerradas!',
			count
		)
		self.message_user(request, msg % count)
	encerrar_aposta.short_description = 'Encerrar Aposta'

class JogoLoteriaAdmin(admin.ModelAdmin):
	#exclude = ('data_pagamento',)
	actions = ['pagar']
	search_fields = ('participante', 'aposta')
	list_display = ('participante', 'aposta', 'pago')

	def pagar(self, request, queryset):
		count = queryset.update(pago=True) #data_pagamento=datetime.today().date()
		msg = ungettext(
			u'%d Jogo Pago!',
			u'%d Jogos pagos!',
			count
		)
		self.message_user(request, msg % count)
	pagar.short_description = 'Pagar Jogos'


class JogoBolaoAdmin(admin.ModelAdmin):
	actions = ['pagar',]
	search_fields = ('participante', 'aposta',)
	list_display = ('participante', 'aposta', 'resultado_A', 'resultado_B', 'pago',)

	def resultado_A(self, obj):
		return '%s: %d' % (obj.aposta.timeA, obj.gols_timeA)
	resultado_A.short_description = 'Aposta A'

	def resultado_B(self, obj):
		return '%s: %d' % (obj.aposta.timeB, obj.gols_timeB)

	resultado_B.short_description = 'Aposta B'

	def pagar(self, request, queryset):
		count = queryset.update(pago=True) #data_pagamento=datetime.today().date()
		msg = ungettext(
			u'%d Jogo Pago!',
			u'%d Jogos pagos!',
			count
		)
		self.message_user(request, msg % count)
	pagar.short_description = 'Pagar Jogos'

class ApostaBolaoAdmin(admin.ModelAdmin):
	pass
		


admin.site.register(ApostaLoteria, ApostaLoteriaAdmin)
admin.site.register(JogoLoteria, JogoLoteriaAdmin)
admin.site.register(JogoBolao, JogoBolaoAdmin)
admin.site.register(ApostaBolao, ApostaBolaoAdmin)