from django.conf.urls import patterns, include, url
from apostas_boloes import core


urlpatterns = patterns('apostas_boloes.core.views',
	url(r'^$', 'home', name='home'),
	url(r'^login$', 'login' , name='login'),
	url(r'^logout$', 'logout', name='logout')
)