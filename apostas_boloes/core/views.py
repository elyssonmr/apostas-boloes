from django.contrib import auth
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.core.urlresolvers import reverse


def home(request):
	return render(request, 'core/home.html')


def login(request): 
	if request.method == 'POST':		
		username = request.POST['username']
		password = request.POST['password']
		user = auth.authenticate(username=username, password=password)
		if user is not None:
			auth.login(request, user)
			print request.GET
			if request.POST.has_key('next'):
				return HttpResponseRedirect(request.POST['next'])
			return HttpResponseRedirect(reverse('core:home'))
		else:
			return render(request, 'login.html',{'error': True})
	return render(request, 'login.html', request.GET)

def logout(request):
	auth.logout(request)
	return HttpResponseRedirect(reverse('core:home'))