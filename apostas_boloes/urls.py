from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^adesao/', include('apostas_boloes.adesao.urls', namespace='adesao')),
    url(r'', include('apostas_boloes.core.urls', namespace='core')),
    url(r'^apostas/', include('apostas_boloes.apostas.urls', namespace='apostas')),
)